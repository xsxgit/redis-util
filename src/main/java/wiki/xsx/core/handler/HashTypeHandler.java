package wiki.xsx.core.handler;

import org.springframework.data.redis.core.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 哈希类型助手
 * @author xsx
 * @date 2019/4/9
 * @since 1.8
 */
public class HashTypeHandler {
    /**
     * 对象模板
     */
    private RedisTemplate<String, Object> redisTemplate;
    /**
     * 字符串模板
     */
    private StringRedisTemplate stringRedisTemplate;
    /**
     * 对象模板
     */
    private HashOperations<String, String, Object> hashOperations;
    /**
     * 字符串模板
     */
    private HashOperations<String, String, String> stringHashOperations;

    /**
     * 哈希类型助手构造
     * @param redisTemplate 对象模板
     * @param stringRedisTemplate 字符串模板
     */
    private HashTypeHandler(RedisTemplate<String, Object> redisTemplate, StringRedisTemplate stringRedisTemplate) {
        this.redisTemplate = redisTemplate;
        this.stringRedisTemplate = stringRedisTemplate;
        this.hashOperations = redisTemplate.opsForHash();
        this.stringHashOperations = stringRedisTemplate.opsForHash();
    }

    /**
     * 获取实例
     * @param redisTemplate 对象模板
     * @param stringRedisTemplate 字符串模板
     * @return 返回实例
     */
    public static HashTypeHandler getInstance(RedisTemplate<String, Object> redisTemplate, StringRedisTemplate stringRedisTemplate) {
        return new HashTypeHandler(redisTemplate, stringRedisTemplate);
    }

    /**
     * 存入对象
     * <p>HSET key field value</p>
     * @since redis 2.0.0
     * @param key 键
     * @param hashKey hash键
     * @param value 对象
     */
    public void putAsObj(String key, String hashKey, Object value) {
        this.hashOperations.put(key, hashKey, value);
    }

    /**
     * 存入字符串
     * <p>HSET key field value</p>
     * @since redis 2.0.0
     * @param key 键
     * @param hashKey hash键
     * @param value 字符串
     */
    public void put(String key, String hashKey, String value) {
        this.stringHashOperations.put(key, hashKey, value);
    }

    /**
     * 存入对象如果不存在
     * <p>HSETNX key field value</p>
     * @since redis 2.0.0
     * @param key 键
     * @param hashKey hash键
     * @param value 对象
     * @return 返回布尔值,成功true,失败false
     */
    public Boolean putIfAbsentAsObj(String key, String hashKey, Object value) {
        return this.hashOperations.putIfAbsent(key, hashKey, value);
    }

    /**
     * 存入字符串如果不存在
     * <p>HSETNX key field value</p>
     * @since redis 2.0.0
     * @param key 键
     * @param hashKey hash键
     * @param value 字符串
     * @return 返回布尔值,成功true,失败false
     */
    public Boolean putIfAbsent(String key, String hashKey, String value) {
        return this.stringHashOperations.putIfAbsent(key, hashKey, value);
    }

    /**
     * 存入对象集合
     * <p>HMSET key field value [field value ...]</p>
     * @since redis 2.0.0
     * @param key 键
     * @param map 对象集合
     */
    public void putAllAsObj(String key, Map<String, Object> map) {
        this.hashOperations.putAll(key, map);
    }

    /**
     * 存入字符串集合
     * <p>HMSET key field value [field value ...]</p>
     * @since redis 2.0.0
     * @param key 键
     * @param map 字符串集合
     */
    public void putAll(String key, Map<String, String> map) {
        this.stringHashOperations.putAll(key, map);
    }

    /**
     * 获取对象
     * <p>HGET key field</p>
     * @since redis 2.0.0
     * @param key 键
     * @param hashKey hash键
     * @param <T> 返回类型
     * @return 返回对象
     */
    public <T> T getAsObj(String key, String hashKey) {
        return (T) this.hashOperations.get(key, hashKey);
    }

    /**
     * 获取字符串
     * <p>HGET key field</p>
     * @since redis 2.0.0
     * @param key 键
     * @param hashKey hash键
     * @return 返回字符串
     */
    public String get(String key, String hashKey) {
        return this.stringHashOperations.get(key, hashKey);
    }

    /**
     * 批量获取对象
     * <p>HMGET key field [field ...]</p>
     * @since redis 2.0.0
     * @param key 键
     * @param hashKeys hash键
     * @return 返回对象列表
     */
    public List mgetAsObj(String key, String ...hashKeys) {
        return this.hashOperations.multiGet(key, Arrays.asList(hashKeys));
    }

    /**
     * 批量获取字符串
     * <p>HMGET key field [field ...]</p>
     * @since redis 2.0.0
     * @param key 键
     * @param hashKeys hash键
     * @return 返回字符串列表
     */
    public List<String> mget(String key, String ...hashKeys) {
        return this.stringHashOperations.multiGet(key, Arrays.asList(hashKeys));
    }

    /**
     * 移除对象
     * <p>HDEL key field [field ...]</p>
     * @since redis 2.0.0
     * @param key 键
     * @param hashKeys hash键
     */
    public void removeAsObj(String key, String ...hashKeys) {
        this.hashOperations.delete(key, Arrays.asList(hashKeys).toArray());
    }

    /**
     * 移除字符串
     * <p>HDEL key field [field ...]</p>
     * @since redis 2.0.0
     * @param key 键
     * @param hashKeys hash键
     */
    public void remove(String key, String ...hashKeys) {
        this.stringHashOperations.delete(key, Arrays.asList(hashKeys).toArray());
    }

    /**
     * 获取对象集合
     * <p>HGETALL key</p>
     * @since redis 2.0.0
     * @param key 键
     * @return 返回对象字典
     */
    public Map<String, Object> entriesAsObj(String key) {
        return this.hashOperations.entries(key);
    }

    /**
     * 获取字符串集合
     * <p>HGETALL key</p>
     * @since redis 2.0.0
     * @param key 键
     * @return 返回字符串字典
     */
    public Map<String, String> entries(String key) {
        return this.stringHashOperations.entries(key);
    }

    /**
     * 获取对象hash键集合
     * <p>HKEYS key</p>
     * @since redis 2.0.0
     * @param key 键
     * @return 返回对象字典键集合
     */
    public Set keysAsObj(String key) {
        return this.hashOperations.keys(key);
    }

    /**
     * 获取字符串hash键集合
     * <p>HKEYS key</p>
     * @since redis 2.0.0
     * @param key 键
     * @return 返回字符串字典键集合
     */
    public Set<String> keys(String key) {
        return this.stringHashOperations.keys(key);
    }

    /**
     * 获取对象集合
     * <p>HVALS key</p>
     * @since redis 2.0.0
     * @param key 键
     * @return 返回对象列表
     */
    public List valuesAsObj(String key) {
        return this.hashOperations.values(key);
    }

    /**
     * 获取字符串集合
     * <p>HVALS key</p>
     * @since redis 2.0.0
     * @param key 键
     * @return 返回字符串列表
     */
    public List<String> values(String key) {
        return this.stringHashOperations.values(key);
    }

    /**
     * 获取对象数量
     * <p>HLEN key</p>
     * @since redis 3.2.0
     * @param key 键
     * @return 返回对象数量
     */
    public Long sizeAsObj(String key) {
        return this.hashOperations.size(key);
    }

    /**
     * 获取字符串数量
     * <p>HLEN key</p>
     * @since redis 3.2.0
     * @param key 键
     * @return 返回字符串数量
     */
    public Long size(String key) {
        return this.stringHashOperations.size(key);
    }

    /**
     * 是否包含对象的key
     * <p>HEXISTS key field</p>
     * @since redis 2.0.0
     * @param key 键
     * @param hashKey hash键
     * @return 返回布尔值,存在true,不存在false
     */
    public Boolean hasKeyAsObj(String key, String hashKey) {
        return this.hashOperations.hasKey(key, hashKey);
    }

    /**
     * 是否包含字符串的key
     * <p>HEXISTS key field</p>
     * @since redis 2.0.0
     * @param key 键
     * @param hashKey hash键
     * @return 返回布尔值,存在true,不存在false
     */
    public Boolean hasKey(String key, String hashKey) {
        return this.stringHashOperations.hasKey(key, hashKey);
    }

    /**
     * 自增
     * <p>HINCRBYFLOAT key field increment</p>
     * @since redis 2.6.0
     * @param key 键
     * @param hashKey hash键
     * @param data 步长
     * @return 返回自增后的值
     */
    public Double increment(String key, String hashKey, Double data) {
        return this.stringHashOperations.increment(key, hashKey, data);
    }

    /**
     * 自增
     * <p>HINCRBY key field increment</p>
     * @since redis 2.0.0
     * @param key 键
     * @param hashKey hash键
     * @param data 步长
     * @return 返回自增后的值
     */
    public Long increment(String key, String hashKey, Long data) {
        return this.stringHashOperations.increment(key, hashKey, data);
    }

    /**
     * 自增
     * <p>HINCRBY key field increment</p>
     * @since redis 2.0.0
     * @param key 键
     * @param hashKey hash键
     * @return 返回自增后的值
     */
    public Long increment(String key, String hashKey) {
        return this.stringHashOperations.increment(key, hashKey, 1L);
    }

    /**
     * 递减
     * <p>HINCRBYFLOAT key field increment</p>
     * @since redis 2.6.0
     * @param key 键
     * @param hashKey hash键
     * @param data 步长
     * @return 返回递减后的值
     */
    public Double decrement(String key, String hashKey, Double data) {
        return this.stringHashOperations.increment(key, hashKey, -data);
    }

    /**
     * 递减
     * <p>HINCRBY key field increment</p>
     * @since redis 2.0.0
     * @param key 键
     * @param hashKey hash键
     * @param data 步长
     * @return 返回递减后的值
     */
    public Long decrement(String key, String hashKey, Long data) {
        return this.stringHashOperations.increment(key, hashKey, -data);
    }

    /**
     * 递减
     * <p>HINCRBY key field increment</p>
     * @since redis 2.0.0
     * @param key 键
     * @param hashKey hash键
     * @return 返回递减后的值
     */
    public Long decrement(String key, String hashKey) {
        return this.stringHashOperations.increment(key, hashKey, -1L);
    }

    /**
     * 匹配对象
     * <p>HSCAN key cursor [MATCH pattern] [COUNT count]</p>
     * @since redis 2.8.0
     * @param key 键
     * @param count 数量
     * @param pattern 规则
     * @return 返回匹配对象
     */
    public Cursor<Map.Entry<String, Object>> scanAsObj(String key, Long count, String pattern) {
        return this.hashOperations.scan(key, ScanOptions.scanOptions().count(count).match(pattern).build());
    }

    /**
     * 匹配字符串
     * <p>HSCAN key cursor [MATCH pattern] [COUNT count]</p>
     * @since redis 2.8.0
     * @param key 键
     * @param count 数量
     * @param pattern 规则
     * @return 返回匹配字符串
     */
    public Cursor<Map.Entry<String, String>> scan(String key, Long count, String pattern) {
        return this.stringHashOperations.scan(key, ScanOptions.scanOptions().count(count).match(pattern).build());
    }

    /**
     * 获取spring redis模板
     * @return 返回对象模板
     */
    public RedisTemplate getRedisTemplate() {
        return this.redisTemplate;
    }

    /**
     * 获取spring string redis模板
     * @return 返回字符串模板
     */
    public StringRedisTemplate getStringRedisTemplate() {
        return this.stringRedisTemplate;
    }
}
