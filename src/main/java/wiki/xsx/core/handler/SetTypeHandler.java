package wiki.xsx.core.handler;

import org.springframework.data.redis.core.*;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * 无序集合类型助手
 * @author xsx
 * @date 2019/4/15
 * @since 1.8
 */
public class SetTypeHandler {
    /**
     * 对象模板
     */
    private RedisTemplate<String, Object> redisTemplate;
    /**
     * 字符串模板
     */
    private StringRedisTemplate stringRedisTemplate;
    /**
     * 对象模板
     */
    private SetOperations<String, Object> setOperations;
    /**
     * 字符串模板
     */
    private SetOperations<String, String> stringSetOperations;

    /**
     * 无序集合类型助手构造
     * @param redisTemplate 对象模板
     * @param stringRedisTemplate 字符串模板
     */
    private SetTypeHandler(RedisTemplate<String, Object> redisTemplate, StringRedisTemplate stringRedisTemplate) {
        this.redisTemplate = redisTemplate;
        this.stringRedisTemplate = stringRedisTemplate;
        this.setOperations = redisTemplate.opsForSet();
        this.stringSetOperations = stringRedisTemplate.opsForSet();
    }

    /**
     * 获取实例
     * @param redisTemplate 对象模板
     * @param stringRedisTemplate 字符串模板
     * @return 返回实例
     */
    public static SetTypeHandler getInstance(RedisTemplate<String, Object> redisTemplate, StringRedisTemplate stringRedisTemplate) {
        return new SetTypeHandler(redisTemplate, stringRedisTemplate);
    }

    /**
     * 新增对象
     * <p>SADD key member [member ...]</p>
     * @param key 键
     * @since redis 1.0.0
     * @param values 对象
     * @return 返回成功个数
     */
    public Long addAsObj(String key, Object ...values) {
        return this.setOperations.add(key, values);
    }

    /**
     * 新增字符串
     * <p>SADD key member [member ...]</p>
     * @since redis 1.0.0
     * @param key 键
     * @param values 字符串
     * @return 返回成功个数
     */
    public Long add(String key, String ...values) {
        return this.stringSetOperations.add(key, values);
    }

    /**
     * 弹出对象
     * <p>SPOP key [count]</p>
     * @since redis 1.0.0
     * @param key 键
     * @param <T> 对象类型
     * @return 返回对象
     */
    public <T> T popAsObj(String key) {
        return (T) this.setOperations.pop(key);
    }

    /**
     * 弹出字符串
     * <p>SPOP key [count]</p>
     * @since redis 1.0.0
     * @param key 键
     * @return 返回字符串
     */
    public String pop(String key) {
        return this.stringSetOperations.pop(key);
    }

    /**
     * 弹出对象
     * <p>SPOP key [count]</p>
     * @since redis 3.2.0
     * @param key 键
     * @param count 对象个数
     * @return 返回对象列表
     */
    public Object popAsObj(String key, Long count) {
        return this.setOperations.pop(key);
    }

    /**
     * 弹出字符串
     * <p>SPOP key [count]</p>
     * @since redis 3.2.0
     * @param key 键
     * @param count 字符串个数
     * @return 返回字符串列表
     */
    public String pop(String key, Long count) {
        return this.stringSetOperations.pop(key);
    }

    /**
     * 移除对象
     * <p>SREM key member [member ...]</p>
     * @since redis 1.0.0
     * @param key 键
     * @param values 对象
     * @return 返回移除对象数量
     */
    public Long removeAsObj(String key, Object ...values) {
        return this.setOperations.remove(key, values);
    }

    /**
     * 移除字符串
     * <p>SREM key member [member ...]</p>
     * @since redis 1.0.0
     * @param key 键
     * @param values 字符串
     * @return 返回移除字符串数量
     */
    public Long remove(String key, String ...values) {
        return this.stringSetOperations.remove(key, Arrays.asList(values).toArray());
    }

    /**
     * 移动对象
     * <p>SMOVE source destination member</p>
     * @since redis 1.0.0
     * @param key 键
     * @param destKey 目标键
     * @param value 对象
     * @return 返回布尔值,成功true,失败false
     */
    public Boolean moveAsObj(String key, String destKey, Object value) {
        return this.setOperations.move(key, value, destKey);
    }

    /**
     * 移动字符串
     * <p>SMOVE source destination member</p>
     * @since redis 1.0.0
     * @param key 键
     * @param destKey 目标键
     * @param value 字符串
     * @return 返回布尔值,成功true,失败false
     */
    public Boolean move(String key, String destKey, String value) {
        return this.stringSetOperations.move(key, value, destKey);
    }

    /**
     * 获取对象数量
     * <p>SCARD key</p>
     * @since redis 1.0.0
     * @param key 键
     * @return 返回对象数量
     */
    public Long sizeAsObj(String key) {
        return this.setOperations.size(key);
    }

    /**
     * 获取字符串数量
     * <p>SCARD key</p>
     * @since redis 1.0.0
     * @param key 键
     * @return 返回字符串数量
     */
    public Long size(String key) {
        return this.stringSetOperations.size(key);
    }

    /**
     * 是否包含对象
     * <p>SISMEMBER key member</p>
     * @since redis 1.0.0
     * @param key 键
     * @param value 对象
     * @return 返回布尔值,存在true,不存在false
     */
    public Boolean containsAsObj(String key, Object value) {
        return this.setOperations.isMember(key, value);
    }

    /**
     * 是否包含字符串
     * <p>SISMEMBER key member</p>
     * @since redis 1.0.0
     * @param key 键
     * @param value 字符串
     * @return 返回布尔值,存在true,不存在false
     */
    public Boolean contains(String key, String value) {
        return this.stringSetOperations.isMember(key, value);
    }

    /**
     * 获取不重复的随机对象
     * <p>SRANDMEMBER key [count]</p>
     * @since redis 2.6.0
     * @param key 键
     * @param count 数量
     * @return 返回不重复的随机对象集合
     */
    public Set distinctRandomMembersAsObj(String key, Long count) {
        return this.setOperations.distinctRandomMembers(key, count);
    }

    /**
     * 获取不重复的随机字符串
     * <p>SRANDMEMBER key [count]</p>
     * @since redis 2.6.0
     * @param key 键
     * @param count 数量
     * @return 返回不重复的随机字符串集合
     */
    public Set<String> distinctRandomMembers(String key, Long count) {
        return this.stringSetOperations.distinctRandomMembers(key, count);
    }

    /**
     * 获取可重复的随机对象
     * <p>SRANDMEMBER key [count]</p>
     * @since redis 2.6.0
     * @param key 键
     * @param count 数量
     * @return 返回不重复的随机对象集合
     */
    public List randomMembersAsObj(String key, Long count) {
        return this.setOperations.randomMembers(key, count);
    }

    /**
     * 获取可重复的随机字符串
     * <p>SRANDMEMBER key [count]</p>
     * @since redis 2.6.0
     * @param key 键
     * @param count 数量
     * @return 返回不重复的随机字符串集合
     */
    public List<String> randomMembers(String key, Long count) {
        return this.stringSetOperations.randomMembers(key, count);
    }

    /**
     * 获取可重复的随机对象
     * <p>SRANDMEMBER key [count]</p>
     * @since redis 2.6.0
     * @param key 键
     * @param <T> 对象类型
     * @return 返回可重复的随机对象
     */
    public <T> T randomMemberAsObj(String key) {
        return (T) this.setOperations.randomMember(key);
    }

    /**
     * 获取可重复的随机字符串
     * <p>SRANDMEMBER key [count]</p>
     * @since redis 2.6.0
     * @param key 键
     * @return 返回可重复的随机字符串
     */
    public String randomMember(String key) {
        return this.stringSetOperations.randomMember(key);
    }

    /**
     * 获取对象集合
     * <p>SMEMBERS key</p>
     * @since redis 1.0.0
     * @param key 键
     * @return 返回对象集合
     */
    public Set membersAsObj(String key) {
        return this.setOperations.members(key);
    }

    /**
     * 获取字符串集合
     * <p>SMEMBERS key</p>
     * @since redis 1.0.0
     * @param key 键
     * @return 返回字符串集合
     */
    public Set<String> members(String key) {
        return this.stringSetOperations.members(key);
    }

    /**
     * 取对象差集
     * <p>SDIFF key [key ...]</p>
     * @since redis 1.0.0
     * @param key 键
     * @param otherKys 其他键
     * @return 返回与其他集合的对象差集
     */
    public Set differenceAsObj(String key, String ...otherKys) {
        return this.setOperations.difference(key, Arrays.asList(otherKys));
    }

    /**
     * 取字符串差集
     * <p>SDIFF key [key ...]</p>
     * @since redis 1.0.0
     * @param key 键
     * @param otherKys 其他键
     * @return 返回与其他集合的字符串差集
     */
    public Set<String> difference(String key, String ...otherKys) {
        return this.stringSetOperations.difference(key, Arrays.asList(otherKys));
    }

    /**
     * 取对象差集并存储到新的集合
     * <p>SDIFFSTORE destination key [key ...]</p>
     * @since redis 1.0.0
     * @param key 键
     * @param storeKey 存储键
     * @param otherKys 其他键
     * @return 返回差集对象个数
     */
    public Long differenceAndStoreAsObj(String key, String storeKey, String ...otherKys) {
        return this.setOperations.differenceAndStore(key, Arrays.asList(otherKys), storeKey);
    }

    /**
     * 取字符串差集并存储到新的集合
     * <p>SDIFFSTORE destination key [key ...]</p>
     * @since redis 1.0.0
     * @param key 键
     * @param storeKey 存储键
     * @param otherKys 其他键
     * @return 返回差集字符串个数
     */
    public Long differenceAndStore(String key, String storeKey, String ...otherKys) {
        return this.stringSetOperations.differenceAndStore(key, Arrays.asList(otherKys), storeKey);
    }

    /**
     * 取对象交集
     * <p>SINTER key [key ...]</p>
     * @since redis 1.0.0
     * @param key 键
     * @param otherKys 其他键
     * @return 返回与其他集合的对象交集
     */
    public Set intersectAsObj(String key, String ...otherKys) {
        return this.setOperations.intersect(key, Arrays.asList(otherKys));
    }

    /**
     * 取字符串交集
     * <p>SINTER key [key ...]</p>
     * @since redis 1.0.0
     * @param key 键
     * @param otherKys 其他键
     * @return 返回与其他集合的字符串交集
     */
    public Set<String> intersect(String key, String ...otherKys) {
        return this.stringSetOperations.intersect(key, Arrays.asList(otherKys));
    }

    /**
     * 取对象交集并存储到新的集合
     * <p>SINTERSTORE destination key [key ...]</p>
     * @since redis 1.0.0
     * @param key 键
     * @param storeKey 存储键
     * @param otherKys 其他键
     * @return 返回交集对象个数
     */
    public Long intersectAndStoreAsObj(String key, String storeKey, String ...otherKys) {
        return this.setOperations.intersectAndStore(key, Arrays.asList(otherKys), storeKey);
    }

    /**
     * 取字符串交集并存储到新的集合
     * <p>SINTERSTORE destination key [key ...]</p>
     * @since redis 1.0.0
     * @param key 键
     * @param storeKey 存储键
     * @param otherKys 其他键
     * @return 返回交集字符串个数
     */
    public Long intersectAndStore(String key, String storeKey, String ...otherKys) {
        return this.stringSetOperations.intersectAndStore(key, Arrays.asList(otherKys), storeKey);
    }

    /**
     * 取对象并集
     * <p>SUNION key [key ...]</p>
     * @since redis 1.0.0
     * @param key 键
     * @param otherKys 其他键
     * @return 返回与其他集合的对象交集
     */
    public Set unionAsObj(String key, String ...otherKys) {
        return this.setOperations.union(key, Arrays.asList(otherKys));
    }

    /**
     * 取字符串并集
     * <p>SUNION key [key ...]</p>
     * @since redis 1.0.0
     * @param key 键
     * @param otherKys 其他键
     * @return 返回与其他集合的字符串交集
     */
    public Set<String> union(String key, String ...otherKys) {
        return this.stringSetOperations.union(key, Arrays.asList(otherKys));
    }

    /**
     * 取对象并集并存储到新的集合
     * <p>SUNIONSTORE destination key [key ...]</p>
     * @since redis 1.0.0
     * @param key 键
     * @param storeKey 存储键
     * @param otherKys 其他键
     * @return 返回并集对象个数
     */
    public Long unionAndStoreAsObj(String key, String storeKey, String ...otherKys) {
        return this.setOperations.unionAndStore(key, Arrays.asList(otherKys), storeKey);
    }

    /**
     * 取字符串并集并存储到新的集合
     * <p>SUNIONSTORE destination key [key ...]</p>
     * @since redis 1.0.0
     * @param key 键
     * @param storeKey 存储键
     * @param otherKys 其他键
     * @return 返回并集字符串个数
     */
    public Long unionAndStore(String key, String storeKey, String ...otherKys) {
        return this.stringSetOperations.unionAndStore(key, Arrays.asList(otherKys), storeKey);
    }

    /**
     * 匹配对象
     * <p>SSCAN key cursor [MATCH pattern] [COUNT count]</p>
     * @since redis 2.8.0
     * @param key 键
     * @param count 数量
     * @param pattern 规则
     * @return 返回匹配对象
     */
    public Cursor scanAsObj(String key, Long count, String pattern) {
        return this.setOperations.scan(key, ScanOptions.scanOptions().count(count).match(pattern).build());
    }

    /**
     * 匹配字符串
     * <p>SSCAN key cursor [MATCH pattern] [COUNT count]</p>
     * @since redis 2.8.0
     * @param key 键
     * @param count 数量
     * @param pattern 规则
     * @return 返回匹配字符串
     */
    public Cursor<String> scan(String key, Long count, String pattern) {
        return this.stringSetOperations.scan(key, ScanOptions.scanOptions().count(count).match(pattern).build());
    }

    /**
     * 获取spring redis模板
     * @return 返回对象模板
     */
    public RedisTemplate getRedisTemplate() {
        return this.redisTemplate;
    }

    /**
     * 获取spring string redis模板
     * @return 返回字符串模板
     */
    public StringRedisTemplate getStringRedisTemplate() {
        return this.stringRedisTemplate;
    }
}
