package wiki.xsx.core.handler;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 字符串类型助手
 * @author xsx
 * @date 2019/4/8
 * @since 1.8
 */
public class StringTypeHandler {
    /**
     * 对象模板
     */
    private RedisTemplate<String, Object> redisTemplate;
    /**
     * 字符串模板
     */
    private StringRedisTemplate stringRedisTemplate;
    /**
     * 对象模板
     */
    private ValueOperations<String, Object> operations;
    /**
     * 字符串模板
     */
    private ValueOperations<String, String> stringOperations;

    /**
     * 字符串类型助手构造
     * @param redisTemplate 对象模板
     * @param stringRedisTemplate 字符串模板
     */
    private StringTypeHandler(RedisTemplate<String, Object> redisTemplate, StringRedisTemplate stringRedisTemplate) {
        this.redisTemplate = redisTemplate;
        this.stringRedisTemplate = stringRedisTemplate;
        this.operations = redisTemplate.opsForValue();
        this.stringOperations = stringRedisTemplate.opsForValue();
    }

    /**
     * 获取实例
     * @param redisTemplate 对象模板
     * @param stringRedisTemplate 字符串模板
     * @return 返回实例
     */
    public static StringTypeHandler getInstance(RedisTemplate<String, Object> redisTemplate, StringRedisTemplate stringRedisTemplate) {
        return new StringTypeHandler(redisTemplate, stringRedisTemplate);
    }

    /**
     * 移除对象
     * <p>DEL key [key ...]</p>
     * @since redis 1.0.0
     * @param keys 键
     */
    public void removeAsObj(String ...keys) {
        this.operations.getOperations().delete(Arrays.asList(keys));
    }

    /**
     * 移除字符串
     * <p>DEL key [key ...]</p>
     * @since redis 1.0.0
     * @param keys 键
     */
    public void remove(String ...keys) {
        this.stringOperations.getOperations().delete(Arrays.asList(keys));
    }

    /**
     * 设置对象
     * <p>SET key value [expiration EX seconds|PX milliseconds] [NX|XX]</p>
     * @since redis 2.0.0
     * @param key 键
     * @param value 对象
     */
    public void setAsObj(String key, Object value) {
        this.operations.set(key, value);
    }

    /**
     * 设置字符串
     * <p>SET key value [expiration EX seconds|PX milliseconds] [NX|XX]</p>
     * @since redis 2.0.0
     * @param key 键
     * @param value 字符串
     */
    public void set(String key, String value) {
        this.stringOperations.set(key, value);
    }

    /**
     * 设置对象(若存在则更新过期时间)
     * <p>SETEX key seconds value</p>
     * @since redis 2.0.0
     * @param key 键
     * @param value 对象
     * @param timeout 过期时间
     * @param unit 时间单位
     */
    public void setAsObj(String key, Object value, long timeout, TimeUnit unit) {
        this.operations.set(key, value, timeout, unit);
    }

    /**
     * 设置字符串(若存在则更新过期时间)
     * <p>SETEX key seconds value</p>
     * @since redis 2.0.0
     * @param key 键
     * @param value 字符串
     * @param timeout 过期时间
     * @param unit 时间单位
     */
    public void set(String key, String value, long timeout, TimeUnit unit) {
        this.stringOperations.set(key, value, timeout, unit);
    }

    /**
     * 批量设置对象
     * <p>MSET key value [key value ...]</p>
     * @since redis 1.0.1
     * @param map 对象集合
     */
    public void msetAsObj(Map<String, Object> map) {
        this.operations.multiSet(map);
    }

    /**
     * 批量设置字符串
     * <p>MSET key value [key value ...]</p>
     * @since redis 1.0.1
     * @param map 字符串集合
     */
    public void mset(Map<String, String> map) {
        this.stringOperations.multiSet(map);
    }

    /**
     * 追加新字符串
     * <p>APPEND key value</p>
     * @since redis 2.0.0
     * @param key 键
     * @param value 字符串
     */
    public void append(String key, String value) {
        this.stringOperations.append(key, value);
    }

    /**
     * 设置对象如果不存在
     * <p>SETNX key value</p>
     * @since redis 1.0.0
     * @param key 键
     * @param value 对象
     * @return 返回布尔值,成功true,失败false
     */
    public Boolean setIfAbsentAsObj(String key, Object value) {
        return this.operations.setIfAbsent(key, value);
    }

    /**
     * 设置字符串如果不存在
     * <p>SETNX key value</p>
     * @since redis 1.0.0
     * @param key 键
     * @param value 字符串
     * @return 返回布尔值,成功true,失败false
     */
    public Boolean setIfAbsent(String key, String value) {
        return this.stringOperations.setIfAbsent(key, value);
    }

    /**
     * 批量设置对象如果不存在
     * <p>MSETNX key value [key value ...]</p>
     * @since redis 1.0.1
     * @param map 对象集合
     * @return 返回布尔值,成功true,失败false
     */
    public Boolean msetIfAbsentAsObj(Map<String, Object> map) {
        return this.operations.multiSetIfAbsent(map);
    }

    /**
     * 批量设置字符串如果不存在
     * <p>MSETNX key value [key value ...]</p>
     * @since redis 1.0.1
     * @param map 字符串集合
     * @return 返回布尔值,成功true,失败false
     */
    public Boolean msetIfAbsent(Map<String, String> map) {
        return this.stringOperations.multiSetIfAbsent(map);
    }

    /**
     * 获取对象
     * <p>GET key</p>
     * @since redis 1.0.0
     * @param key 键
     * @param <T> 返回类型
     * @return 返回对象
     */
    public <T> T getAsObj(String key) {
        return (T) this.operations.get(key);
    }

    /**
     * 获取字符串
     * <p>GET key</p>
     * @since redis 1.0.0
     * @param key 键
     * @return 返回字符串
     */
    public String get(String key) {
        return this.stringOperations.get(key);
    }

    /**
     * 获取并设置新对象
     * <p>GETSET key value</p>
     * @since redis 1.0.0
     * @param key 键
     * @param value 对象
     * @param <T> 返回类型
     * @return 返回对象
     */
    public <T> T getAndSetAsObj(String key, Object value) {
        return (T) this.operations.getAndSet(key, value);
    }

    /**
     * 获取并设置新字符串
     * <p>GETSET key value</p>
     * @since redis 1.0.0
     * @param key 键
     * @param value 字符串
     * @return 返回字符串
     */
    public String getAndSet(String key, String value) {
        return this.stringOperations.getAndSet(key, value);
    }

    /**
     * 批量获取对象
     * <p>MGET key [key ...]</p>
     * @since redis 1.0.0
     * @param keys 键
     * @return 返回对象列表
     */
    public List mgetAsObj(String ...keys) {
        return this.operations.multiGet(Arrays.asList(keys));
    }

    /**
     * 批量获取字符串
     * <p>MGET key [key ...]</p>
     * @since redis 1.0.0
     * @param keys 键
     * @return 返回字符串列表
     */
    public List<String> mget(String ...keys) {
        return this.stringOperations.multiGet(Arrays.asList(keys));
    }

    /**
     * 获取字符串的长度
     * <p>STRLEN key</p>
     * @since redis 2.2.0
     * @param key 键
     * @return 返回字符串长度
     */
    public Long length(String key) {
        return this.stringOperations.size(key);
    }

    /**
     * 自增
     * <p>INCRBYFLOAT key increment</p>
     * @since redis 2.6.0
     * @param key 键
     * @param data 步长
     * @return 返回自增后的值
     */
    public Double increment(String key, Double data) {
        return this.stringOperations.increment(key, data);
    }

    /**
     * 自增
     * <p>INCRBY key increment</p>
     * @since redis 1.0.0
     * @param key 键
     * @param data 步长
     * @return 返回自增后的值
     */
    public Long increment(String key, Long data) {
        return this.stringOperations.increment(key, data);
    }

    /**
     * 自增
     * <p>INCR key</p>
     * @since redis 1.0.0
     * @param key 键
     * @return 返回自增后的值
     */
    public Long increment(String key) {
        return this.stringOperations.increment(key, 1L);
    }

    /**
     * 递减
     * <p>DECRBY key decrement</p>
     * @since redis 1.0.0
     * @param key 键
     * @param data 步长
     * @return 返回递减后的值
     */
    public Long decrement(String key, Long data) {
        return this.stringOperations.increment(key, -data);
    }

    /**
     * 递减
     * <p>DECR key</p>
     * @since redis 1.0.0
     * @param key 键
     * @return 返回递减后的值
     */
    public Long decrement(String key) {
        return this.stringOperations.increment(key, -1);
    }

    /**
     * 获取spring redis模板
     * @return 返回对象模板
     */
    public RedisTemplate getRedisTemplate() {
        return this.redisTemplate;
    }

    /**
     * 获取spring string redis模板
     * @return 返回字符串模板
     */
    public StringRedisTemplate getStringRedisTemplate() {
        return this.stringRedisTemplate;
    }
}
