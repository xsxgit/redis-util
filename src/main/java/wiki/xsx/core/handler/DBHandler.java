package wiki.xsx.core.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * 数据库助手
 * @author xsx
 * @date 2019/4/25
 * @since 1.8
 */
public class DBHandler {

    private static final Logger log = LoggerFactory.getLogger(DBHandler.class.getName());

    /**
     * 对象模板
     */
    private RedisTemplate<String, Object> redisTemplate;
    /**
     * 字符串模板
     */
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 数据库助手构造
     * @param redisTemplate 对象模板
     * @param stringRedisTemplate 字符串模板
     */
    private DBHandler(RedisTemplate<String, Object> redisTemplate, StringRedisTemplate stringRedisTemplate) {
        this.redisTemplate = redisTemplate;
        this.stringRedisTemplate = stringRedisTemplate;
    }

    /**
     * 获取实例
     * @param redisTemplate 对象模板
     * @param stringRedisTemplate 字符串模板
     * @return 返回实例
     */
    public static DBHandler getInstance(RedisTemplate<String, Object> redisTemplate, StringRedisTemplate stringRedisTemplate) {
        return new DBHandler(redisTemplate, stringRedisTemplate);
    }

    /**
     * 获取当前数据库索引
     * @return 返回当前数据库索引
     */
    public int getDB() {
        int index = 0;
        RedisConnectionFactory redisConnectionFactory;
        try {
            redisConnectionFactory = this.redisTemplate.getConnectionFactory();
        }catch (IllegalStateException ex) {
            redisConnectionFactory = this.stringRedisTemplate.getConnectionFactory();
        }
        if (redisConnectionFactory instanceof LettuceConnectionFactory) {
            LettuceConnectionFactory factory = (LettuceConnectionFactory) redisConnectionFactory;
            index = factory.getDatabase();
        }else if (redisConnectionFactory instanceof JedisConnectionFactory) {
            JedisConnectionFactory factory = (JedisConnectionFactory) redisConnectionFactory;
            index = factory.getDatabase();
        }else {
            throw new RuntimeException("no support connection factory");
        }
        return index;
    }

    /**
     * 设置当前数据库索引
     * @since redis 1.0.0
     * @param dbIndex 数据库索引
     */
    public void setDB(int dbIndex) {
        this.redisTemplate.setConnectionFactory(CommonHandler.getConnectionFactory(dbIndex));
        this.stringRedisTemplate.setConnectionFactory(CommonHandler.getConnectionFactory(dbIndex));
    }

    /**
     * 获取原有数据库索引并设置新索引
     * @since redis 1.0.0
     * @param dbIndex 数据库索引
     * @return 返回原有索引
     */
    public int getAndSetDB(int dbIndex) {
        int index = getDB();
        setDB(dbIndex);
        return index;
    }

    /**
     * 获取spring redis模板
     * @return 返回对象模板
     */
    public RedisTemplate getRedisTemplate() {
        return this.redisTemplate;
    }

    /**
     * 获取spring string redis模板
     * @return 返回字符串模板
     */
    public StringRedisTemplate getStringRedisTemplate() {
        return this.stringRedisTemplate;
    }
}
