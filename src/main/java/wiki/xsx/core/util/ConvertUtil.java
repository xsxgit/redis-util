package wiki.xsx.core.util;

import org.springframework.data.redis.core.DefaultTypedTuple;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.data.redis.serializer.RedisSerializer;

import java.util.*;

/**
 * 转换工具
 * @author xsx
 * @date 2019/4/28
 * @since 1.8
 */
public class ConvertUtil {

    /**
     * 转为字节数组
     * @param keys 键
     * @return 返回二位字节数组
     */
    public static byte[][] toArray(RedisSerializer serializer, String ...keys) {
        byte[][] bytes = new byte[keys.length][];
        for (int i = 0; i < keys.length; i++) {
            bytes[i] = serializer.serialize(keys[i]);
        }
        return bytes;
    }

    /**
     * 将字典转为TypedTuple类型的集合
     * @param map 字典
     * @param <T> 对象类型
     * @return 返回TypedTuple类型的集合
     */
    public static  <T> Set<ZSetOperations.TypedTuple<T>> toTypedTupleSet(Map<Double, T> map) {
        if(map==null){
            return null;
        }
        Set<ZSetOperations.TypedTuple<T>> set = new HashSet<>(map.size());
        for (Map.Entry<Double, T> entry : map.entrySet()) {
            set.add(new DefaultTypedTuple<>(entry.getValue(), entry.getKey()));
        }
        return set;
    }

    /**
     * 将TypedTuple类型的集合转为字典
     * @param set TypedTuple类型的集合
     * @param <T> 对象类型
     * @return 返回TypedTuple类型的集合
     */
    public static <T> Map<Double, T> toMap(Set<ZSetOperations.TypedTuple<T>> set) {
        if(set==null){
            return null;
        }
        Map<Double, T> map = new LinkedHashMap<>(set.size());
        for (ZSetOperations.TypedTuple<T> typedTuple : set) {
            map.put(typedTuple.getScore(), typedTuple.getValue());
        }
        return map;
    }

    /**
     * 将对象转为字典
     * @param values 对象
     * @param <T> 对象类型
     * @return 返回对象字典
     */
    @SafeVarargs
    public static <T> Map<Double, T> toMap(T ...values) {
        if(values==null){
            return null;
        }
        Map<Double, T> map = new HashMap<>(values.length);
        for (int i = 0; i < values.length; i++) {
            map.put((double) i, values[i]);
        }
        return map;
    }
}
