package wiki.xsx.core.util;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import wiki.xsx.core.handler.*;

/**
 * redis工具
 * @author xsx
 * @date 2019/4/8
 * @since 1.8
 */
public class RedisUtil {

    /**
     * 通用助手实例
     */
    private static final CommonHandler COMMON_HANDLER = CommonHandler.getInstance();

    /**
     * 获取数据库助手
     * @return 返回数据库助手
     */
    public static DBHandler getDBHandler() {
        return COMMON_HANDLER.getDBHandler(COMMON_HANDLER.getDefaultKey());
    }

    /**
     * 获取数据库助手
     * @param dbIndex 数据库索引
     * @return 返回数据库助手
     */
    public static DBHandler getDBHandler(int dbIndex) {
        return COMMON_HANDLER.getDBHandler(String.valueOf(dbIndex));
    }

    /**
     * 获取键助手
     * @return 返回键助手
     */
    public static KeyHandler getKeyHandler() {
        return COMMON_HANDLER.getKeyHandler(COMMON_HANDLER.getDefaultKey());
    }

    /**
     * 获取键助手
     * @param dbIndex 数据库索引
     * @return 返回键助手
     */
    public static KeyHandler getKeyHandler(int dbIndex) {
        return COMMON_HANDLER.getKeyHandler(String.valueOf(dbIndex));
    }

    /**
     * 获取字符串类型助手
     * @return 返回字符串类型助手
     */
    public static StringTypeHandler getStringTypeHandler() {
        return COMMON_HANDLER.getStringTypeHandler(COMMON_HANDLER.getDefaultKey());
    }

    /**
     * 获取字符串类型助手
     * @param dbIndex 数据库索引
     * @return 返回字符串类型助手
     */
    public static StringTypeHandler getStringTypeHandler(int dbIndex) {
        return COMMON_HANDLER.getStringTypeHandler(String.valueOf(dbIndex));
    }

    /**
     * 获取哈希类型助手
     * @return 返回哈希类型助手
     */
    public static HashTypeHandler getHashTypeHandler() {
        return COMMON_HANDLER.getHashTypeHandler(COMMON_HANDLER.getDefaultKey());
    }

    /**
     * 获取哈希类型助手
     * @param dbIndex 数据库索引
     * @return 返回哈希类型助手
     */
    public static HashTypeHandler getHashTypeHandler(int dbIndex) {
        return COMMON_HANDLER.getHashTypeHandler(String.valueOf(dbIndex));
    }

    /**
     * 获取列表类型助手
     * @return 返回列表类型助手
     */
    public static ListTypeHandler getListTypeHandler() {
        return COMMON_HANDLER.getListTypeHandler(COMMON_HANDLER.getDefaultKey());
    }

    /**
     * 获取列表类型助手
     * @param dbIndex 数据库索引
     * @return 返回列表类型助手
     */
    public static ListTypeHandler getListTypeHandler(int dbIndex) {
        return COMMON_HANDLER.getListTypeHandler(String.valueOf(dbIndex));
    }

    /**
     * 获取无序集合类型助手
     * @return 返回无序集合类型助手
     */
    public static SetTypeHandler getSetTypeHandler() {
        return COMMON_HANDLER.getSetTypeHandler(COMMON_HANDLER.getDefaultKey());
    }

    /**
     * 获取无序集合类型助手
     * @param dbIndex 数据库索引
     * @return 返回无序集合类型助手
     */
    public static SetTypeHandler getSetTypeHandler(int dbIndex) {
        return COMMON_HANDLER.getSetTypeHandler(String.valueOf(dbIndex));
    }

    /**
     * 获取有序集合类型助手
     * @return 返回有序集合类型助手
     */
    public static ZsetTypeHandler getZsetTypeHandler() {
        return COMMON_HANDLER.getZsetTypeHandler(COMMON_HANDLER.getDefaultKey());
    }

    /**
     * 获取有序集合类型助手
     * @param dbIndex 数据库索引
     * @return 返回有序集合类型助手
     */
    public static ZsetTypeHandler getZsetTypeHandler(int dbIndex) {
        return COMMON_HANDLER.getZsetTypeHandler(String.valueOf(dbIndex));
    }

    /**
     * 获取基数类型助手
     * @return 返回基数类型助手
     */
    public static HyperLogLogTypeHandler getHyperLogLogTypeHandler() {
        return COMMON_HANDLER.getHyperLogLogTypeHandler(COMMON_HANDLER.getDefaultKey());
    }

    /**
     * 获取基数类型助手
     * @param dbIndex 数据库索引
     * @return 返回基数类型助手
     */
    public static HyperLogLogTypeHandler getHyperLogLogTypeHandler(int dbIndex) {
        return COMMON_HANDLER.getHyperLogLogTypeHandler(String.valueOf(dbIndex));
    }

    /**
     * 获取位图类型助手
     * @return 返回位图类型助手
     */
    public static BitmapTypeHandler getBitmapHandler() {
        return COMMON_HANDLER.getBitmapTypeHandler(COMMON_HANDLER.getDefaultKey());
    }

    /**
     * 获取位图类型助手
     * @param dbIndex 数据库索引
     * @return 返回位图类型助手
     */
    public static BitmapTypeHandler getBitmapHandler(int dbIndex) {
        return COMMON_HANDLER.getBitmapTypeHandler(String.valueOf(dbIndex));
    }

    /**
     * 获取默认的对象模板
     * @return 返回对象模板
     */
    public static RedisTemplate<String, Object> getDefaultRedisTemplate() {
        return COMMON_HANDLER.getDefaultRedisTemplate();
    }

    /**
     * 获取默认的字符串模板
     * @return 返回字符串模板
     */
    public static StringRedisTemplate getDefaultStringRedisTemplate() {
        return COMMON_HANDLER.getDefaultStringRedisTemplate();
    }
}
