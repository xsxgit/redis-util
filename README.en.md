# redis-util

#### Description
Integrate Redistplate and String Redistplate to provide a more friendly API and make it easier to use

#### Software Architecture
Dependency spring-data-redis 1.5.2.RELEASE version，compatible spring 4.0

#### Installation

```
<dependency>
    <groupId>wiki.xsx</groupId>
    <artifactId>redis-util</artifactId>
    <version>1.0.0</version>
</dependency>
```

#### Document
https://apidoc.gitee.com/xsxgit/redis-util

#### Instructions
quickly start

one way：
```
<bean class="wiki.xsx.core.config.RedisAutoConfiguration"/>
```

two way：
```
@Bean
public RedisAutoConfiguration redisAutoConfiguration(){
    return new RedisAutoConfiguration();
}
```

default config(redis.properties)：
```
#info
redis.database=0
redis.url=
redis.host=localhost
redis.port=6379
redis.password=
redis.timeout=1000
redis.ssl=false


#pool
#redis.pool.usePool=true
#redis.pool.jedisConfig=false
#redis.pool.lettuceConfig=true
#redis.pool.maxIdle=8
#redis.pool.minIdle=0
#redis.pool.maxActive=8
#redis.pool.maxWait=-1
#redis.pool.shutdownTimeout=100


#sentinel
redis.sentinel.master=
redis.sentinel.nodes=


#cluster
redis.cluster.maxRedirects=
redis.cluster.nodes=
```

Use method wiki.xsx.core.util.RedisUtil.getXXXHandler to get instance for type 

1. getStringTypeHandler：get instance for string type
2. getHashTypeHandler： get instance for hash type
3. getListTypeHandler： get instance for list type
4. getSetTypeHandler： get instance for set type
5. getZsetTypeHandler：get instance for zset type
6. getHyperLogLogTypeHandler：get instance for hyperLogLog type 
7. getBitmapHandler：get instance for bitmap type 
8. getDBHandler：get instance for db
9. getKeyHandler：get instance for key


**Specifically: XXXAsObj is an object type serialization correlation method, XXX is a string type serialization correlation method**

@since is a method supported by redis version, such as @since redis 1.0.0 for redis version 1.0.0

#### spring-boot version
https://gitee.com/xsxgit/spring-boot-starter-fast-redis